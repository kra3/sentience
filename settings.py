#Minimum battery level to get notification
BATTERY_MIN_HOUR = '00'
BATTERY_MIN_MIN = '15'
#BATTERY_MIN_SEC = '00'

#General settings
NOTIFICATION_DURATION = '15000'  # time to live in millisecond
NOTIFICATION_SPAN = 900  # once every X seconds
BREAK_TIME = 300  # break time in between notifications in seconds
# in effective, it would be NOTIFICATION_SPAN+ BREAK_TIME, the time for each sprint
